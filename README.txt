ad_auction README.txt

INSTALLATION
install modules
ad_auction also needs the ad and ad_owners module as well as ubercart and probably cck.  Ad_oweners is necessary so that users can edit their ads.
Enable permissions for users to create, edit own and show advertisements
When you are ready and understand how the module works, then enable it at admin/settings/ad_auction
WARNING: ad_auction creates ad slots for every ad channel in your system on each cron run.  Therefore if you have a lot of ad channels already then this may cause a lot of ad slots to be created.  Ad_auction is currently only configured to work for ALL ads on a site.

CONFIGURATION
Create ad_slot ctype
Use ubercart product classes to make ad_slot ctype a product
Give your users permission to create advertisements
Set permissions for roles to administer ads - all other roles will not see the channel or group settings in ad forms

AD AUCTION SETTINGS
admin/settings/ad_auction
Enter the required settings here, ensuring that the auction period is greater than the number of days to run each auction.  The auction period determines when the Advertisement will actually be displayed so time is required after the auction expiry for the user to make the purchase and for the cron to run and create the next lot of ad slots.

AD AUCTION FUNCTIONALITY
Each channel will create a corresponding ad slot node when inserted whether through the ad admin UI or during the cron run.  Once created, the ad slot details cannot be changed except for the group.  Note that for each channel created or existing in the ad admin area there will be created a new ad slot for each period determined at ad_auction admin: admin/settings/ad_auction

The ad module works as follows:
Create an ad group for each block you wish to display ads in
Create channels for each ad group for each different slot you want to have in each block and set their url rules.  When creating a channel you also have to nominate a group for that channel.  This is so that the group and channel can both be transmitted to the ad assigned to the slot when purchased.

It is important to allow a few days between the expiry of an auction and the start of the next ad cycle for the user to purchase their ad slot and for the cron to run and create the next lot of ad slots for the following period.  Currently the only period available is 1 month. 

The same ad can be used in any number of ad slots.