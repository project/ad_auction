<?php
/**
 * Implementation of hook_ca_predicate().
 */
function ad_auction_ca_predicate() {
  // Set the node status to published when a payment is received
  // and the balance is less than or equal to 0.
  $configurations['ad_auction_received'] = array(
    '#title' => t('Publish advertisement on full payment'),
    '#description' => t('Only happens when a payment is entered and the balance is <= $0.00.'),
    '#class' => 'payment',
    '#trigger' => 'uc_payment_entered',
    '#status' => 1,
    '#conditions' => array(
      '#operator' => 'AND',
      '#conditions' => array(
        array(
          '#name' => 'uc_payment_condition_order_balance',
          '#title' => t('If the balance is less than or equal to $0.00.'),
          '#argument_map' => array(
            'order' => 'order',
          ),
          '#settings' => array(
            'negate' => FALSE,
            'balance_comparison' => 'less_equal',
          ),
        ),
      ),
    ),
    '#actions' => array(
      array(
        '#name' => 'ad_auction_publish_ad',
        '#title' => t('Publish the advertisement in the purchased ad slot.'),
        '#argument_map' => array(
          'order' => 'order',
        ),
      ),
    ),
  );

  return $configurations;
}
function ad_auction_ca_action() {
  $order_arg = array(
    '#entity' => 'uc_order',
    '#title' => t('Order'),
  );
  $actions['ad_auction_publish_ad'] = array(
    '#title' => t('Publish Advertisement'),
    '#category' => t('Drupal'),
    '#callback' => 'ad_auction_publish_advert',
    '#arguments' => array(
      'order' => $order_arg,
    ),
  );

  return $actions;
}

function ad_auction_publish_advert($order) {
  //drupal_set_message('ad slot purchase '.print_r($order,1));
  //  We publish the selected advertisement into each ad slot purchased and set our default values
  foreach($order->products as $product) {
	$node = node_load($product->nid);
	if($node->type=="ad_slot") {
	  //  An add can appear in more than one channel and an ad might be in a channel at the time of this purchase but that al slot may expire before the new ad slot starts
	  $ad = db_fetch_object(db_query("SELECT * FROM {ad_auction} WHERE ad_nid=%d AND nid=%d", $product->data['advert_nid'], $product->nid));
	  //if(!$ad->chid>0) $ad = db_fetch_object(db_query("SELECT * FROM {ad_auction} WHERE nid=%d", $product->nid));
	  // We set the status to 1 in the ad auction record which tells us that this slot has been bought and that is all we need do at this stage.  Later this record will be processed in the cron run
	  //  However we might need to add a script for when the user purchases the ad slot after the cron has processed the new cycle
	  drupal_set_message('updating purchase for '.$product->nid);
	  db_query("UPDATE {ad_auction} SET status=1, order_id=%d WHERE nid=%d", $order->order_id, $product->nid);
	}
  }
  
}